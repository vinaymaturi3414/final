from django.contrib import admin
from django.urls import path,include
# from shoppingapp.views import LogoutView
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/',obtain_auth_token),
    # path('logout/', LogoutView.as_view(), name='auth_logout'),
    path('',include('shoppingapp.urls')),
]
